#! /usr/bin/python3
import psutil,time,sys, json, socket

# la = psutil.getloadavg()
# cpu = la[0] / psutil.cpu_count() * 100
cpu = psutil.cpu_percent(interval=1)
ram = int(psutil.virtual_memory().percent)
disk = int(psutil.disk_usage('/').percent)
sUptime = int(time.time()) - int(psutil.boot_time())

hostname = socket.gethostname()

if sUptime < 86400 :
    dUptime = 0
else:
    dUptime = sUptime // 86400

hUptime = sUptime // 3600 % 24

if cpu > 100 :
    cpu = 100

mUptime = sUptime % 3600 // 60

totalCpu = psutil.cpu_count()
totalRam =  int(psutil.virtual_memory().total / 1024 / 1024)
totalDisk = int(psutil.disk_usage('/').total / 1024 / 1024 / 1000)

r = {"cpu": cpu, "ram": ram, "disk": disk, "d": dUptime, "h": hUptime, "m": mUptime, "hostname": hostname, "totalDisk": totalDisk, "totalCpu": totalCpu, "totalRam": totalRam}


print(json.dumps(r))

