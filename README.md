##### Install

```
wget lemp.net.ru/setup.sh && bash setup.sh
passwd lemp
su - lemp
```

##### SSL

`sudo certbot certonly --webroot --non-interactive --agree-tos --email admin@lemp.net.ru -d dev.lemp.net.ru -w /var/www/html`

##### Create Site HTTP
```
sudo ansible-playbook ~/ansible/site-create.yml --extra-vars="\
username=lemp \
userpass=Pass123123 \
db_pass=Pass123123 \
domain=dev.lemp.net.ru \
email=admin@dev.lemp.net.ru \
directory=public \
php=8.1 \
app=''"
```
##### Create Site HTTPS
```
sudo ansible-playbook ~/ansible/site-create.yml --extra-vars="\
username=lemp \
userpass=Pass123123 \
db_pass=Pass123123 \
domain=dev.lemp.net.ru \
email=admin@dev.lemp.net.ru \
nginx_vhost=vhost-ssl \
ssl_cert=/etc/letsencrypt/localhost.crt \
ssl_key=/etc/letsencrypt/localhost.key \
directory=public \
php=8.1 \
app=''"
```
##### Update Site
```
sudo ansible-playbook ~/ansible/site-create.yml --extra-vars="\
username=lemp \
userpass=Pass123123 \
db_pass=Pass123123 \
domain=dev.lemp.net.ru \
email=admin@dev.lemp.net.ru \
nginx_vhost=vhost-ssl \
ssl_cert=/etc/letsencrypt/localhost.crt \
ssl_key=/etc/letsencrypt/localhost.key \
directory=public \
php=8.1"
```
##### Delete Site

`sudo ansible-playbook ~/ansible/site-delete.yml --extra-vars="username=lemp"`
